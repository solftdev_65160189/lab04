/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04;

/**
 *
 * @author gamme
 */
public class Table {
    private String[] table;

    public Table() {
        table = new String[9];
        createNumTable();
    }

    public void createNumTable() {
        for (int a = 0; a <= 8; a++) {
            String as = String.valueOf(a + 1);
            table[a] = as;
        }
    }

    public void printTable() {
        int b = 0;
        for (int a = 0; a <= 8; a++) {
            b = b + 1;
            if (b > 3) {
                b = 1;
                System.out.println("");
            }
            System.out.print(table[a] + "  ");
            if (a == 8) {
                System.out.println("");
            }
        }
    }

    public String[] getTable() {
        return table;
    }

    public void setCellValue(int position, String value) {
        if (position >= 0 && position < table.length) {
            table[position] = value;
        }
    }
}

