/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04;

/**
 *
 * @author gamme
 */
public class Player {
    private int player_id;
    private String player_name;

    public Player(int player_id, String player_name) {
        this.player_id = player_id;
        this.player_name = player_name;
    }

    public String getPlayerName() {
        return player_name;
    }

    public void setPlayerName(String name) {
        this.player_name = name;
    }

    public int getPlayerId() {
        return player_id;
    }

    public void setPlayerId(int id) {
        this.player_id = id;
    }
}

